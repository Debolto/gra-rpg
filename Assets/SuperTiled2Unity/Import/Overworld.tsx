<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.0" name="Overworld" tilewidth="16" tileheight="16" tilecount="1440" columns="40">
 <image source="Overworld.png" width="640" height="576"/>
 <tile id="86">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4.03125" y="0" width="11.9063" height="15.9688"/>
  </objectgroup>
 </tile>
 <tile id="87">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16.0625" height="16"/>
  </objectgroup>
 </tile>
 <tile id="88">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.03125" y="0" width="16.0625" height="15.9688"/>
  </objectgroup>
 </tile>
 <tile id="89">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0625" y="0.03125" width="15.9688" height="15.9063"/>
  </objectgroup>
 </tile>
 <tile id="90">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="13.0313" height="16"/>
  </objectgroup>
 </tile>
 <tile id="126">
  <objectgroup draworder="index" id="3">
   <object id="2" x="3.09375" y="0.0625" width="12.9063" height="15.9375"/>
  </objectgroup>
 </tile>
 <tile id="127">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16.0313" height="15.9688"/>
  </objectgroup>
 </tile>
 <tile id="128">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0.0781" y="0.0781" width="15.9063" height="15.9063"/>
  </objectgroup>
 </tile>
 <tile id="129">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="-0.03125" width="16.0313" height="16.0313"/>
  </objectgroup>
 </tile>
 <tile id="130">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="0.03125" width="12.9688" height="16.0313"/>
  </objectgroup>
 </tile>
 <tile id="166">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7" y="0" width="9.03125" height="12.9688"/>
  </objectgroup>
 </tile>
 <tile id="167">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="0.09375" width="15.9688" height="11.875"/>
  </objectgroup>
 </tile>
 <tile id="168">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="0.0625" width="16.0625" height="10.9063"/>
  </objectgroup>
 </tile>
 <tile id="169">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0625" y="0.03125" width="15.9375" height="11.9688"/>
  </objectgroup>
 </tile>
 <tile id="170">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0625" y="0.03125" width="8.9375" height="13.0625"/>
  </objectgroup>
 </tile>
 <tile id="176">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0.03125" width="15.9063" height="15.9063"/>
  </objectgroup>
 </tile>
 <tile id="177">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0625" y="0.03125" width="15.9375" height="16"/>
  </objectgroup>
 </tile>
 <tile id="178">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16.0313" height="16.0625"/>
  </objectgroup>
 </tile>
 <tile id="179">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="15.9688" height="16"/>
  </objectgroup>
 </tile>
 <tile id="180">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="0.03125" width="15.9688" height="15.9375"/>
  </objectgroup>
 </tile>
 <tile id="207">
  <objectgroup draworder="index" id="4">
   <object id="3" x="0.8125" y="2.0625" width="14.2813" height="11.0313"/>
  </objectgroup>
 </tile>
 <tile id="208">
  <objectgroup draworder="index" id="3">
   <object id="2" x="3" y="2.9375" width="9" height="1.96875"/>
   <object id="4" x="1.96875" y="5" width="12" height="3"/>
   <object id="5" x="1" y="7.90625" width="13.9688" height="5.0625"/>
   <object id="6" x="2.9375" y="13.0313" width="11.125" height="1.90625"/>
  </objectgroup>
 </tile>
 <tile id="209">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.96875" y="4" width="12" height="2.34375"/>
   <object id="2" x="0.9375" y="6.34375" width="11.9063" height="3.59375"/>
   <object id="3" x="0.9375" y="10" width="14.0625" height="2.90625"/>
  </objectgroup>
 </tile>
 <tile id="217">
  <objectgroup draworder="index" id="3">
   <object id="3" x="0.0625" y="0" width="16.0313" height="12.0313"/>
  </objectgroup>
 </tile>
 <tile id="218">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="0.03125" width="16" height="12.0625"/>
  </objectgroup>
 </tile>
 <tile id="219">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0625" y="0.03125" width="15.9688" height="11.9063"/>
  </objectgroup>
 </tile>
 <tile id="220">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0.03125" width="15.9688" height="11.9688"/>
  </objectgroup>
 </tile>
 <tile id="242">
  <objectgroup draworder="index" id="2">
   <object id="1" x="11" y="11" width="5.0625" height="5.0625"/>
  </objectgroup>
 </tile>
 <tile id="243">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="15.9688" height="4.09375"/>
   <object id="2" x="2" y="11" width="10.9688" height="2.40625"/>
   <object id="3" x="5.03125" y="10.0313" width="6.96875" height="1.8125"/>
  </objectgroup>
 </tile>
 <tile id="244">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="11" width="5.09375" height="5.1875"/>
  </objectgroup>
 </tile>
 <tile id="282">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="-0.1875" width="7.28125" height="17.6875"/>
   <object id="3" x="9.96875" y="3" width="3.90625" height="6"/>
  </objectgroup>
 </tile>
 <tile id="283">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.09375" y="-0.25" width="16.1875" height="16.375"/>
  </objectgroup>
 </tile>
 <tile id="284">
  <objectgroup draworder="index" id="3">
   <object id="4" x="-0.15625" y="-0.0625" width="4.15625" height="16.375"/>
  </objectgroup>
 </tile>
 <tile id="322">
  <objectgroup draworder="index" id="3">
   <object id="4" x="10.9688" y="-0.28125" width="6.65625" height="5.25"/>
  </objectgroup>
 </tile>
 <tile id="323">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.21875" y="-0.25" width="17.8438" height="4.1875"/>
   <object id="2" x="2.75" y="3.40625" width="8.25" height="1.5"/>
   <object id="3" x="7.03125" y="4.53125" width="1.9375" height="1.40625"/>
  </objectgroup>
 </tile>
 <tile id="324">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.28125" y="-0.28125" width="5.21875" height="5.1875"/>
  </objectgroup>
 </tile>
 <tile id="362">
  <objectgroup draworder="index" id="4">
   <object id="5" x="-0.59375" y="-0.78125" width="5.5625" height="17.4688"/>
   <object id="6" x="-0.125" y="-0.28125" width="18.7813" height="4.21875"/>
  </objectgroup>
 </tile>
 <tile id="363">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.375" y="-0.375" width="23.4375" height="5.34375"/>
   <object id="2" x="12.0313" y="-0.78125" width="5.0625" height="18.0938"/>
  </objectgroup>
 </tile>
 <tile id="402">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.125" y="-0.21875" width="4.875" height="17.0313"/>
   <object id="5" x="-0.625" y="12.0625" width="18" height="4.59375"/>
  </objectgroup>
 </tile>
 <tile id="403">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.0625" y="-0.40625" width="6.96875" height="17.0938"/>
   <object id="2" x="-1.03125" y="10.9063" width="18.3438" height="6.65625"/>
  </objectgroup>
 </tile>
 <tile id="449">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.0625" y="6" width="8.875" height="1.9375"/>
   <object id="2" x="4.0625" y="7.96875" width="11.9375" height="4.9375"/>
   <object id="3" x="2.96875" y="12.9375" width="13.0313" height="3.0625"/>
  </objectgroup>
 </tile>
 <tile id="450">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="7" width="10.9375" height="2.9375"/>
   <object id="2" x="0" y="9.96875" width="13.0313" height="6.09375"/>
   <object id="3" x="5.0625" y="5.9375" width="1.875" height="1.03125"/>
  </objectgroup>
 </tile>
 <tile id="489">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.90625" y="0.0625" width="14.0313" height="12"/>
  </objectgroup>
 </tile>
 <tile id="490">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="-0.03125" width="14.0313" height="12.9688"/>
  </objectgroup>
 </tile>
 <tile id="680">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.95652" y="1.95652" width="4.26087" height="14.0435"/>
  </objectgroup>
 </tile>
 <tile id="682">
  <objectgroup draworder="index" id="4">
   <object id="8" x="5.04348" y="3.98098" width="8.82609" height="12.0625"/>
   <object id="9" x="13.913" y="7" width="2.13179" height="7.13043"/>
  </objectgroup>
 </tile>
 <tile id="683">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.04348" y="4.00679" width="5.91304" height="11.9063"/>
   <object id="2" x="0.0869565" y="7" width="4.82609" height="7.00679"/>
  </objectgroup>
 </tile>
 <tile id="684">
  <objectgroup draworder="index" id="2">
   <object id="3" x="0.0557065" y="4.03125" width="15.8139" height="9.99321"/>
  </objectgroup>
 </tile>
 <tile id="720">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6.08696" y="0.0434783" width="4" height="14.0435"/>
  </objectgroup>
 </tile>
 <tile id="722">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.08696" y="0.0434783" width="5.95652" height="13.913"/>
   <object id="2" x="11.1304" y="7" width="4.91304" height="7"/>
  </objectgroup>
 </tile>
 <tile id="723">
  <objectgroup draworder="index" id="2">
   <object id="4" x="5" y="0" width="5.95652" height="13.913"/>
   <object id="5" x="0.0869565" y="7" width="4.86957" height="6.91304"/>
  </objectgroup>
 </tile>
 <tile id="724">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.91304" y="0.0434783" width="4.13043" height="16"/>
  </objectgroup>
 </tile>
 <tile id="760">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4.86957" y="4.02446" width="6.13043" height="9.9755"/>
   <object id="2" x="11.087" y="7.04348" width="4.95652" height="6.95652"/>
  </objectgroup>
 </tile>
 <tile id="761">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.04348" y="4.03804" width="5.92527" height="9.962"/>
   <object id="2" x="0.0434783" y="7" width="4.95652" height="7.08696"/>
  </objectgroup>
 </tile>
</tileset>
