using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public Rigidbody2D rb;
    public Animator animator;
    Vector2 movement;

    public InventoryObject inventory;
    private void Update()
    {
        //Input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);

		if (Input.GetKeyDown(KeyCode.Space))
		{
            inventory.Save();
		}
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            inventory.Load();
        }

    }

    void FixedUpdate()
	{
        //Movement
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
        var item = other.GetComponent<GroundItem>();
        if (item)
        {
            inventory.AddItem(new Item (item.item), 1);
            Destroy(other.gameObject);
        }
    }
	public void OnApplicationQuit()
	{
        inventory.Container.Items = new InventorySlot[35];
	}


}
