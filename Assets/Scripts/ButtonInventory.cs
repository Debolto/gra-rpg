using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ButtonInventory : MonoBehaviour
{
	public Animator animator;
	public Canvas uimenu;

	void Start()
	{
		animator = gameObject.GetComponent<Animator>();
		GameObject tempObject = GameObject.Find("UIMenu");
		if(tempObject != null)
		{
			uimenu = tempObject.GetComponent<Canvas>();
			if(uimenu == null)
			{
				Debug.Log("Could not locate Canvas component on " + tempObject.name);
			}
		}
		
	}
/* void Update()
    {
		if (Input.GetKeyDown(KeyCode.I))
		{
			if (uimenu == true)
			{
				OnButtonClickActive();
				Debug.Log("UIMenu show");
			}
			else if(uimenu.enabled == false)
			{
				ButtonClickDeactive();
				Debug.Log("UIMenu hide");
			}
			else
			{
				Debug.Log("I'm stuck");
			}
		}
    }*/

	void OnButtonClickActive()
	{
		Debug.Log("I want to show");
		animator.Play("menu_showing");
	}
	void ButtonClickDeactive()
	{
		Debug.Log("I want to hide");
		animator.Play("menu_hidden");
	}
}
