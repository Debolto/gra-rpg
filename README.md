Update 4.0:
- Poprawa ekwipunku - stworzona baza danych pod przedmioty, każdy przedmiot ma teraz własne ID
- Poprawa ekwipunku - jeżeli podniesiemy jeden przedmiot z wybranym ID, nie pokaże się ilość, dopiero przy 2 takim samym przedmiocie
- Poprawa ekwipunku - poprawiony system zbierania (przedmiot potrafi odnaleźć slot i dodać się do niego)
- Poprawa ekwipunku - generowanie slotów przy starcie
- Poprawa ekwipunku - można zamieniać miejscami przedmioty
- Prototyp AI - niestety to prototyp ponieważ pathfinding jest użyty z cudzej paczki na potrzebę nauki działania

Update 4.1:
- Rozpoczęcie prac przy ustawieniach

W kolejnym update:
-Stworzenie własnego AI od podstaw
-Dodanie możliwości zakładania przedmiotów na postać
-Dodanie możliwości używania przedmiotów z dolnych slotów użytkowych
-Dodanie systemu walki



